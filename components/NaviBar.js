import React, { useContext } from 'react';
import Link from 'next/link';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import UserContext from '../UserContext';

export default function NaviBar() {

	const { user } = useContext(UserContext);

	return (
		<Navbar bg="info" expand="lg">
		  <Link href="/">
		  	<a className="navbar-brand text-white">Budgetarian Tracker</a>
		  </Link>
		  <Navbar.Toggle aria-controls="basic-navbar-nav" />
		  <Navbar.Collapse id="basic-navbar-nav">
		    <Nav className="ml-auto">

	            {(user.token !==null) 
	            	? 
	            		<React.Fragment>
	            		    <Link href="/categories">
	            		        <a className="nav-link text-white" role="button">Category</a>
	            		    </Link>	            		
	            		    <Link href="/record/history">
	            		        <a className="nav-link text-white" role="button">Transaction Overview</a>
	            		    </Link>
	            		    <Link href="/category-breakdown">
	            		        <a className="nav-link text-white" role="button">Breakdown</a>
	            		    </Link>	     
	            		    <Link href="/balance-trend">
	            		        <a className="nav-link text-white" role="button">Trend</a>
	            		    </Link>	 	            		           		    
	            		    <Link href="/logout">
	            		        <a className="nav-link text-white" role="button">Logout</a>
	            		    </Link>
	            		</React.Fragment>
	            		:
	            	<React.Fragment>
	            	</React.Fragment>
	
	            }
		    </Nav>
		  </Navbar.Collapse>
		</Navbar>
	)
}