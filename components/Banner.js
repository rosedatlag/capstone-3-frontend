import { Jumbotron } from 'react-bootstrap';
import Link from 'next/Link';

export default function Banner({data}){

	console.log(data);
	const { title } = data;

	return(
		<Jumbotron>
			<h1>{title}</h1>
		</Jumbotron>
	)
}

// props {
// 	data: {}
// }