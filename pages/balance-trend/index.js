import React, { useContext, useState, useEffect } from 'react';
import { Line } from 'react-chartjs-2';
import { Form } from 'react-bootstrap';
import moment from 'moment';
import AppHelper from '../../app-helper';

//import View from '../components/View';

export default function balancetrend() {

	const [balances, setBalances] = useState ([]);
	const [labels, setLabels] = useState ([]);
	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');

	
	useEffect (() => {

	const options = {
        headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`}
    }

	    fetch(`${AppHelper.API_URL}/records`, options)
	    .then(AppHelper.toJSON)
	    .then(data=>{ 

	    	console.log(data)

	    	let currentBalances = []
	    	let labelName = []
	    	data.forEach(record => {

		    	if (startDate !== "" && endDate !== "") {
		    		if (moment(record.tranDate).isBetween(startDate, endDate, null, '[]') == true) {
			    		console.log(record)
			    		console.log(currentBalances)
			    		currentBalances.push(record.tranAmount)
			    		labelName.push(record.catName)
		    		}
		    	} else {

		    			console.log(record)
			    		console.log(currentBalances)
			    		currentBalances.push(record.tranAmount)
			    		labelName.push(record.catName)
		    	}


	    	})
	    	setLabels(labelName)
	    	setBalances(currentBalances)
		})

	} ,[startDate, endDate])

	const data = {
		labels: labels,
		datasets: [
			{
				label: 'Trend',
				backgroundColor: 'pink',
				borderColor: 'gray',
				borderWidth: 1,
				hoverBackgroundColor: 'blue',
				hoverBorderColor: 'darkblue',
				data: balances
			}
		]
	}
	return (
		<React.Fragment>
			<h3 className="text-center">Budget Trend</h3>
      <br></br>

      			<Form.Group controlId="startDate">
      				<Form.Label>From: </Form.Label>
                    <Form.Control 
                        type="date"
                        onChange={(e) => setStartDate(e.target.value)}
                        required
                    />
            </Form.Group>
            <Form.Group controlId="endDate">
            		<Form.Label>To: </Form.Label>
                    <Form.Control 
                        type="date" 
                        onChange={(e) => setEndDate(e.target.value)}
                        required
                    />
            </Form.Group>			
			<Line data={data} />
		</React.Fragment>
		)
}