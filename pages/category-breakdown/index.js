import React, { useContext, useState, useEffect } from 'react';
import { Pie } from 'react-chartjs-2';
import moment from 'moment';
import { Form } from 'react-bootstrap';
import getRandomColor from 'randomcolor';
import AppHelper from '../../app-helper';
//import View from '../components/View';

export default function categorybreakdown() {

	const [records, setRecords] = useState ([]);
	const [categories, setCategories] = useState ([]);
	const [categoryTotal, setCategoryTotal] = useState ([]);
	const [labels, setLabels] = useState ([]);
	const [bgColors, setBgColors] = useState([]);
	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');

	
	useEffect (() => {

		const options = {
	        headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`}
	    }

		    fetch(`${AppHelper.API_URL}/records`, options)
		    .then(AppHelper.toJSON)
		    .then(data=>{
		    	console.log(data)
				setRecords(data)
			})

	} ,[])

	useEffect(() => {
	    fetch(`${AppHelper.API_URL}/categories`, {
	      headers:
	      {
	        Authorization: `Bearer ${localStorage.getItem('token')}`
	      }
	    })
	    .then(AppHelper.toJSON)
	    .then(data => {
	    	console.log(data)
           		setCategories(data)
           })
	}, [])

	useEffect (() => {

		let total = [];
		let dates = [];

		categories.forEach(category => {

			let totalPrice = 0

			records.map(record => {

				if (startDate !== "" && endDate !=="") {
					if(moment(record.tranDate).isBetween(startDate, endDate, null, '[]') == true){
						if(category.catName === record.catName) {
							totalPrice = totalPrice + record.tranAmount
							dates.push(record.tranDate)
						}
					}
				} else {
					if(category.catName === record.catName) {
						totalPrice = totalPrice + record.tranAmount
						dates.push(record.tranDate)
					}
				}
			})
			total.push(totalPrice)
		})
		setCategoryTotal(total)

	},[categories, startDate, endDate])

	useEffect(() => {

		console.log(categories)
		let names = []
		categories.forEach(category => {
			// console.log(category.name)
			names.push(category.catName)
			// console.log(names)
		})

		setLabels(names)


	}, [categories])

	useEffect(() => {

		setBgColors(labels.map(() => getRandomColor()))

	}, [labels])


	const pieChartSettings = {
		datasets: [{
			data: categoryTotal,
			cutout: 0,
			backgroundColor: bgColors
			
		}],
		labels:  labels
	}

	return (
		<React.Fragment>

		<h3 className="text-center">Category Breakdown</h3>
   
      <br></br>

      			<Form.Group controlId="startDate">
 				<Form.Label>From: </Form.Label>     			
                    <Form.Control 
                        type="date"
                        onChange={(e) => setStartDate(e.target.value)}
                        required
                    />
            </Form.Group>
            <Form.Group controlId="endDate">
            <Form.Label>To: </Form.Label>
                    <Form.Control 
                        type="date" 
                        onChange={(e) => setEndDate(e.target.value)}
                        required
                    />
            </Form.Group>
			<Pie data ={pieChartSettings} redraw={false}/>
		</React.Fragment>
		)
}