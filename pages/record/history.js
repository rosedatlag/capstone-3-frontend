import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Dropdown, Table, Card } from 'react-bootstrap';
import Router from 'next/router';
import View from '../../components/View';
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';
import moment from 'moment';
// import {Input} from 'mdbreact';
import UserContext from '../../UserContext';

export default function tranhistory() {

    const [categories, setCategories] = useState([]);
    const [records, setRecords] = useState([]);
    
    const [term, setTerm] = useState('');
    const [filter, setFilter] = useState('');

    const [totalBalance, setTotalBalance] = useState('');

    const [catName, setCatName] = useState('');
    const [catType, setCatType] = useState('');
    const [tranName, setTranName] = useState('')
    const [tranAmount, setTranAmount] = useState('');
    const [tranDate, setTranDate] = useState('');

useEffect(() => {

    fetch(`${AppHelper.API_URL}/records`, {
      headers:
      {
        Authorization: `Bearer ${localStorage.getItem('token')}`, "Content-Type": 'application/json'
      }
    }) 
    .then(AppHelper.toJSON)
    .then(data => {

        console.log(data)

        let total = 0;

        data.forEach(record => {

          if(record.catType === "Income") {
            total += record.tranAmount

          }else {
            total -= record.tranAmount

          }
        })

        setTotalBalance(total)


        setRecords(data.map(record => {

            if (record.catType === filter){
              
              
              if (term !== '') {
                                                                    
                    if (record.catName.toLowerCase().includes(term.toLowerCase()) || record.tranName.toLowerCase().includes(term.toLowerCase())){
                        if(record.catType === "Income") {
                            return (
                              <tr key={record._id}>
                                <td>{moment(record.tranDate).format('MM/DD/YYYY')}</td>
                                <td>{record.catName}</td>
                                <td>{record.catType}</td>
                                <td>{record.tranName}</td>
                                <td>+{record.tranAmount}</td>
                              </tr>       
                            )               
                        } else {
                            return (
                              <tr key={record._id}>
                                <td>{moment(record.tranDate).format('MM/DD/YYYY')}</td>
                                <td>{record.catName}</td>
                                <td>{record.catType}</td>
                                <td>{record.tranName}</td>
                                <td>-{record.tranAmount}</td>
                              </tr>       
                            )                                 
                        }                      
                    } else {
                      return null
                    }
              } else {
                  if(record.catType === "Income") {
                      return (
                        <tr key={record._id}>
                          <td>{moment(record.tranDate).format('MM/DD/YYYY')}</td>
                          <td>{record.catName}</td>
                          <td>{record.catType}</td>
                          <td>{record.tranName}</td>
                          <td>+{record.tranAmount}</td>
                        </tr>       
                      )               
                  } else {
                      return (
                        <tr key={record._id}>
                          <td>{moment(record.tranDate).format('MM/DD/YYYY')}</td>
                          <td>{record.catName}</td>
                          <td>{record.catType}</td>
                          <td>{record.tranName}</td>
                          <td>-{record.tranAmount}</td>
                        </tr>       
                      )                                 
                  }                     
              }

            } else if (filter !== "Income" && filter !== "Expense"){
                if (term !== '') {
                                                                    
                    if (record.catName.toLowerCase().includes(term.toLowerCase()) || record.tranName.toLowerCase().includes(term.toLowerCase())){
                        if(record.catType === "Income") {
                            return (
                              <tr key={record._id}>
                                <td>{moment(record.tranDate).format('MM/DD/YYYY')}</td>
                                <td>{record.catName}</td>
                                <td>{record.catType}</td>
                                <td>{record.tranName}</td>
                                <td>+{record.tranAmount}</td>
                              </tr>       
                            )               
                        } else {
                            return (
                              <tr key={record._id}>
                                <td>{moment(record.tranDate).format('MM/DD/YYYY')}</td>
                                <td>{record.catName}</td>
                                <td>{record.catType}</td>
                                <td>{record.tranName}</td>
                                <td>-{record.tranAmount}</td>
                              </tr>       
                            )                                 
                        }                    
                    } else {
                      return null
                    }
              } else {
                  if(record.catType === "Income") {

                      return (
                        <tr key={record._id}>
                          <td>{moment(record.tranDate).format('MM/DD/YYYY')}</td>
                          <td>{record.catName}</td>
                          <td>{record.catType}</td>
                          <td>{record.tranName}</td>
                          <td>+{record.tranAmount}</td>
                        </tr>       
                      )  
                  } else {

                      return (
                        <tr key={record._id}>
                          <td>{moment(record.tranDate).format('MM/DD/YYYY')}</td>
                          <td>{record.catName}</td>
                          <td>{record.catType}</td>
                          <td>{record.tranName}</td>
                          <td>-{record.tranAmount}</td>
                        </tr>       
                      )                                 
                  }    
              }
            }

        }))

    })   

}, [term, filter, totalBalance]) 

return (

    <React.Fragment>

      <h3 className="text-center">Transaction Overview</h3>
      <br></br>
      <Form.Group controlId="search" className = "w-30">
              <Form.Control 
                  type="text"
                  placeholder="Search Record"
                  onChange={(e) => setTerm(e.target.value)}
              />
      </Form.Group>

      <div>
        <select onChange={(e) => setFilter(e.target.value)}>
          <option value="">All</option>
          <option value="Income">Income</option>
          <option value="Expense">Expense</option>
        </select>
      </div>  

      <br></br>

      <Button type="submit" className="btn" variant="primary" href="/record">Add Transaction</Button> 

      <br></br>

      <br></br>

      <h4 className="text-center">Your Current Balance is PHP <span>{totalBalance}</span></h4>

     <br></br>
    <Table bordered hover className="text-center">
      <thead>
        <tr>
          <th>Transaction Date</th>
          <th>Category Name</th>
          <th>Category Type</th>
          <th>Transaction Name</th>
          <th>Transaction Amount</th> 
        </tr>
      </thead>
      <tbody> 
        {records}
      </tbody>
    </Table>      
  </React.Fragment>

    )

}