import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Dropdown, Table, Card } from 'react-bootstrap';
import Router from 'next/router';
import View from '../../components/View';
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';
import moment from 'moment';
// import {Input} from 'mdbreact';
import UserContext from '../../UserContext';

export default function records() {
    const [categories, setCategories] = useState([]);
    const [records, setRecords] = useState([]);
    
    const [term, setTerm] = useState('');
    const [filter, setFilter] = useState('');

    const [currentBalance, setCurrentBalance] = useState('');
    const [totalIncome, setTotalIncome] = useState('');
    const [totalExpense, setTotalExpense] = useState('');

    const [catName, setCatName] = useState('');
    const [catType, setCatType] = useState('');
    const [tranName, setTranName] = useState('')
    const [tranAmount, setTranAmount] = useState('');
    const [tranDate, setTranDate] = useState('');



   function addRecord(e) {

     e.preventDefault();  
  
     const recoptions = {
        method: 'POST',
        headers: { Authorization: `Bearer ${localStorage.getItem('token')}`, "Content-Type": 'application/json'},

        body: JSON.stringify({
          catName: catName,
          catType: catType,
          tranName: tranName,
          tranDate: tranDate,           
          tranAmount: tranAmount
        })

     }

     console.log(recoptions);


     fetch(`${AppHelper.API_URL}/records`, recoptions)
        .then(AppHelper.toJSON)
        .then(data => {

            console.log(data)

            if(data) {
              Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Your transaction has been added successfully!',
                showConfirmButton: true,
              })
              .then(() => {
                setCatType('')
                setCatName('')
                setTranName('')
                setTranDate('')
                setTranAmount('')                
                Router.push('/record/history')
              }) 
            }
          
          

        })

}    
       
useEffect(() => {
  

    fetch(`${AppHelper.API_URL}/categories`, {
      headers:
      {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(AppHelper.toJSON)
    .then(data => {

        console.log(data)


  setCategories(data.map(category => {
          if(category.catType === catType) {
            return <option value={category.catName} key={category._id}>{category.catName}</option>
          }else {
            return null
          }
        }))      
    })   

}, [catType]) 

  return (

      <React.Fragment>

        <h3 className="text-center">New Transaction</h3>
        <p className="text-center">(Please Enter Your Income or Expense Transaction)</p>
   
      <br></br>
        
        <Form onSubmit={(e) => addRecord(e)}>
            <Form.Group>                 
            <Form.Label htmlFor="catType">Category Type: </Form.Label> <Button href="/categories" className="btn" variant="warning" size="sm">Add Category</Button>
                 <Form.Control
                    as="select"
                    placeholder="Select Category"
                    value={catType}
                    onChange={(e) => setCatType(e.target.value)}
                    required>
                    <option value="true">Select Category</option>
                    <option>Income</option>
                    <option>Expense</option>
                  </Form.Control>  
            </Form.Group> 

            <Form.Group>
              <Form.Label htmlFor="catName">Category Name: </Form.Label>
                 <Form.Control
                    as="select"
                    placeholder="Select Category"
                    value={catName}
                    onChange={(e) => setCatName(e.target.value)}
                    required>
                <option value="true">Select Category</option>
                {categories}
              </Form.Control>
            </Form.Group>

            <Form.Group>
              <Form.Label htmlFor="text">Transaction Name:</Form.Label>
              <Form.Control type="text" value={tranName} onChange={(e) => setTranName(e.target.value)} placeholder="Enter Your Transaction" required/>
            </Form.Group>

            <Form.Group>
              <Form.Label htmlFor="tranDate">Transaction Date:</Form.Label>
              <Form.Control type="date" value={tranDate} onChange={(e) => setTranDate(e.target.value)} placeholder="Enter Transaction Date" required/>
            </Form.Group>

            <Form.Group>
              <Form.Label htmlFor="amount">Transaction Amount:</Form.Label>
              <Form.Control type="number" value={tranAmount} onChange={(e) => setTranAmount(e.target.value)} placeholder="Enter amount ₱" required/>
            </Form.Group>

            
            <Button type="submit" className="btn" variant="primary">Submit</Button> 
            
      </Form>

   </React.Fragment>   
  )

}