import React, { useState, useEffect, useContext } from 'react';
import Router from 'next/router';
import Head from 'next/head';
import { Form, Button, Table } from 'react-bootstrap';
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';
// import UserContext from '../UserContext';
import UserContext from '../../UserContext';

export default function categories() {


	const [catName, setCatName] = useState('');
	const [catType, setCatType] = useState('');
	const [isActive, setIsActive] = useState(false);

	const [categories, setCategories] = useState([]);

	console.log(catName);



	function addCategory(e) {

		// Prevents page redirection via form submission
		e.preventDefault();

		//Clear input fields
		setCatName('');
		setCatType('');


		const catoptions = {
            method: 'POST',
            headers: { Authorization: `Bearer ${localStorage.getItem('token')}`, "Content-Type": 'application/json'},

			body: JSON.stringify({
				catName: catName,
				catType: catType				
			})
        }

        console.log(catoptions);

       fetch(`${AppHelper.API_URL}/categories`, catoptions) 
       		.then(AppHelper.toJSON)
       		.then(data => {

	            if(data) {
	              Swal.fire({
	                position: 'center',
	                icon: 'success',
	                title: 'Category has been added successfully!',
	                showConfirmButton: true,
	              })
	              .then(() => {
	                setCatType('')
	                setCatName('')
	                Router.push('/record')
	              }) 
	            }
	       			console.log(data)
	       		})       

	}

   useEffect(() => {
  

    fetch(`${AppHelper.API_URL}/categories`, {
      headers:
      {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    }) 
    .then(AppHelper.toJSON)
    .then(data => {

 
        setCategories(data.map(category => {

        	console.log(category)
  				return (
						<tr key={category._id}>
							<td>{category.catName}</td>
						    <td>{category.catType}</td>
						</tr>    		

				)

        }))

    })   

}, []) 

	return (

	<React.Fragment>		
		<h3 className="text-center">Income/Expense</h3>	

		<Form onSubmit={(e) => addCategory(e)}>
			<Form.Group controlId="catName">
				<Form.Control
					type="text"
					placeholder="Category Name"
					value={catName}
					onChange={(e) => setCatName(e.target.value)}
					required
				/>		
			</Form.Group>

			<Form.Group controlId="catType">
				<Form.Control
					as="select"
					placeholder="Select Category"
					value={catType}
					onChange={(e) => setCatType(e.target.value)}
					required>
					<option value="true">Select Category</option>
					<option>Income</option>
					<option>Expense</option>
				</Form.Control>		
			</Form.Group>		
				 
				<Button variant="primary" type="submit" id="submitBtn">
					Add Category
				</Button>{' '}
				
				<Button href="/record" variant="secondary" type="back">Back</Button>

		</Form>

		<br></br>

		<h3 className="text-center"> Category List </h3>
		<Table bordered hover className="text-center">
			<thead>
				<tr>
					<th>Category Name</th>
					<th>Category Type</th>
				</tr>
			</thead>
			<tbody>	
				{categories}
			</tbody>
		</Table>	
	</React.Fragment>	
	)

}