import React, { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import NaviBar from '../components/NaviBar';
import Login from '../components/Login';
import { UserProvider } from '../UserContext';
import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function MyApp({ Component, pageProps }) {

	// State hook for the user details
	const [user, setUser] = useState({
		token: null,
		isAdmin: null
	})

	// Function for clearing the localStorage
	const unsetUser = () => {

		localStorage.clear();

		// Changes the value of the user state back to it's original value
		setUser({ 
			token: null,
			isAdmin: null
		});

	}

	console.log(user);

	useEffect(() => {

		setUser({
			token: localStorage.getItem('token'),
			// Added a condition to convert the string data type into boolean.
			isAdmin: localStorage.getItem('isAdmin') === 'true'
		})

	}, [])

	return (
		// <h1>Hello world </h1>
		<React.Fragment>
			<UserProvider value={{user, setUser, unsetUser}}>
				<NaviBar />
				<Container className="my-5">
					<Component {...pageProps} />
				</Container>
			</UserProvider>
		</React.Fragment>
	)
}

export default MyApp


// const [records, setRecords] = useState(0);

// useEffect(() => {
// 	fetch('/records')
// 	.then(data => {
// 		setRecords(data)
// 	})

// })

// const recordsOutput = record.map((record) => {
// 	if (filter === "income" && record.categoryType === filter) {
// 		return <Record data={record} />		
// 	}else if (record.categoryType === "expense") {
// 		return <Record data={record} />
// 	} else {
// 		return null
// 	}
// })