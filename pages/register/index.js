import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Router from 'next/router';
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';

export default function register() {

	// State hooks to store the values of the input fields
	const [firstName, setfirstName] = useState('');
	const [lastName, setlastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setmobileNo] =useState('');
	const [password, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// Check if values are successfully binded
	console.log(firstName);
	console.log(lastName);
	console.log(email);
	console.log(mobileNo);
	console.log(password);
	console.log(password2);

	function registerUser(e) {

		// Prevents page redirection via form submission
		e.preventDefault();

		//Clear input fields
		setfirstName('');
		setlastName('');
		setmobileNo('');
		setEmail('');
		setPassword1('');
		setPassword2('');

		const emailverif = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json'},
			body: JSON.stringify({
				email: email				
			})
        }

        fetch(`${AppHelper.API_URL}/users/email-exists`, emailverif)
        	.then(AppHelper.toJSON)
        	.then(data => {
        		console.log(data)

			        if(data === false) {

				        const options = {
				            method: 'POST',
				            headers: { 'Content-Type': 'application/json'},
							body: JSON.stringify({
								firstName: firstName,
								lastName: lastName,
								email: email,
								mobileNo: mobileNo,				
								password: password

							})
				        }

				        fetch(`${AppHelper.API_URL}/users`, options)
				        	.then(AppHelper.toJSON)
				        	.then(data => {
				        		console.log(data)

				        		if(data === true) {	
				                	Swal.fire({
                						position: 'center',
                						icon: 'success',
                						title: 'Thank you for registering!',
                						showConfirmButton: true
              						})      						
									Router.push('/')
								}else {
				                	Swal.fire({
                						position: 'center',
                						icon: 'error',
                						title: 'Something went wrong.',
                						showConfirmButton: true
              						})      						
								}

				        	})


					}else {
				                Swal.fire({
                					position: 'center',
                					icon: 'error',
                					title: 'Duplicate email found.',
                					showConfirmButton: true
              					})      						
					}

			 })        

    }

	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both passwords match
		if((email !== "" && password !== "" && password2 !== "") && (password === password2)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [email, password, password2])

	return (
		<Form onSubmit={(e) => registerUser(e)}>
			<Form.Group controlId="firstname">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="firstname"
					placeholder="Enter First Name"
					value={firstName}
					onChange={(e) => setfirstName(e.target.value)}
					required
				/>		
			</Form.Group>

			<Form.Group controlId="lastname">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="lastname"
					placeholder="Enter Last Name"
					value={lastName}
					onChange={(e) => setlastName(e.target.value)}
					required
				/>		
			</Form.Group>			
				
			<Form.Group controlId="userEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="mobileno">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
					type="number"
					placeholder="Enter Mobile No."
					value={mobileNo}
					onChange={(e) => setmobileNo(e.target.value)}
					required
				/>		
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password}
					onChange={(e) => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify Password"
					value={password2}
					onChange={(e) => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>

			{isActive ? 
				<Button variant="primary" type="submit" id="submitBtn">
					Register
				</Button> 
				:
				<Button variant="danger" type="submit" id="submitBtn" disabled>
					Register
				</Button>
			}
			
			{''} <Button href="/" variant="outline-secondary" type="back">Back</Button>
		</Form>
	)
}