import React, { useContext, useEffect } from 'react';
import Router from 'next/router';
import UserContext from '../../UserContext';

export default function index() {

	const { unsetUser } = useContext(UserContext);

	useEffect(() => {
		// Clears the localStorage of user information
		unsetUser();
		Router.push('/');
	})

	return null

}